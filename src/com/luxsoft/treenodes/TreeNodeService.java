package com.luxsoft.treenodes;

import java.util.*;
import static java.util.stream.Collectors.toSet;

/**
 * Class TreeNodeService - service for processing with TreeNode objects
 */
public class TreeNodeService {
    public Map<Long, Set<TreeNode>> indexTreeNode = new HashMap<>();

    /**
     * Method for generating TreeNode object with the data from the task
     * @return TreeNode - initial data
     */
    public TreeNode generateTreeNodeFromInterview() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(5, new TreeNode(6), new TreeNode(2, new TreeNode(7), new TreeNode(4))),
                new TreeNode(1, new TreeNode(0), new TreeNode(8)));

        return treeNode;
    }
    /**
     * Method for generating TreeNode object with several subtrees with all deepest
     * @return TreeNode - initial data
     */
    public TreeNode generateTreeNodeSeveralSubtreesWithAllDeepest() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(5, new TreeNode(6), new TreeNode(2, new TreeNode(7), new TreeNode(4))),
                new TreeNode(1, new TreeNode(0), new TreeNode(8, new TreeNode(9), new TreeNode(9))));

        return treeNode;
    }

    /**
     * Method for generating another TreeNode object with several subtrees with all deepest
     * and with null value in the one of them
     * @return TreeNode - initial data
     */
    public TreeNode generateTreeNodeWithNullNode() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(5, new TreeNode(6), new TreeNode(2, new TreeNode(7), new TreeNode(4))),
                new TreeNode(1, new TreeNode(0), new TreeNode(8, new TreeNode(9), null)));

        return treeNode;
    }

    /**
     * Method for generating TreeNode object with one node
     * @return TreeNode - initial data
     */
    public TreeNode generateTreeNodeWithOneNode() {
        TreeNode treeNode = new TreeNode(3);

        return treeNode;
    }
    /**
     * Method to find all subtrees with the deepest TreeNode objects
     *
     * @param treeNode (type: TreeNode)
     * @return Set<TreeNode> - all subtrees with the deepest TreeNode objects
     */
    public Set<TreeNode> subtreesWithAllDeepest(TreeNode treeNode) {

        Set<TreeNode> subtreesWithAllDeepest = new HashSet<>();

        generateTreeNodeIndex(treeNode, 0L);

        if (indexTreeNode.size() == 1) {
            subtreesWithAllDeepest.add(treeNode);
            return subtreesWithAllDeepest;
        }

        Long depthMax = indexTreeNode.keySet().stream()
                .max(Long::compareTo).get();

        Set<TreeNode> deepestTreeNodes = indexTreeNode.getOrDefault(depthMax, new HashSet<>());
        Set<TreeNode> previousDeepestTreeNodes = indexTreeNode.getOrDefault(depthMax - 1, new HashSet<>());

        for (TreeNode deepestTreeNode: deepestTreeNodes) {
            Set<TreeNode> treeNodes = previousDeepestTreeNodes.stream()
                    .filter(treeNodeItem -> deepestTreeNode.equals(treeNodeItem.left) || deepestTreeNode.equals(treeNodeItem.right))
                    .collect(toSet());

            subtreesWithAllDeepest.addAll(treeNodes);
        }

        indexTreeNode.clear();
        return subtreesWithAllDeepest;
    }

    /**
     * Method to find depth for the each TreeNode objects withing the current TreeNode object,
     * and generate index by depth and set it to map
     *
     * @param treeNode (type TreeNode) -
     * @param depth (type Long) - depth of the TreeNode object
     * @return ThreeNode
     */
    private TreeNode generateTreeNodeIndex(TreeNode treeNode, Long depth) {
        depth = depth + 1;

        Set<TreeNode> treeNodeSet = indexTreeNode.getOrDefault(depth, new HashSet<>());
        treeNodeSet.add(treeNode);
        indexTreeNode.putIfAbsent(depth, treeNodeSet);

        TreeNode left = treeNode.left;
        TreeNode right = treeNode.right;

        if (left != null) {
            generateTreeNodeIndex(left, depth);
        } else {
            return left;
        }

        if (right != null) {
            generateTreeNodeIndex(right, depth);
        } else {
            return right;
        }

        return treeNode;
    }
}
