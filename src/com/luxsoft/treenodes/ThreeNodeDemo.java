package com.luxsoft.treenodes;

import java.util.Set;

public class ThreeNodeDemo {

    public static void main(String[] args) {
        TreeNodeService treeNodeService = new TreeNodeService();

        TreeNode treeNodeFromInterview = treeNodeService.generateTreeNodeFromInterview();
        Set<TreeNode> result1 = treeNodeService.subtreesWithAllDeepest(treeNodeFromInterview);

        result1.forEach(treeNode -> System.out.println(String.format("[%s, %s, %s]", treeNode.val,
                treeNode.left != null ? treeNode.left.val : "null",
                treeNode.right != null ? treeNode.right.val : "null")));
        System.out.println("-------------------------");

        TreeNode treeNodeWithSeveralSubtreesWithAllDeepest = treeNodeService.generateTreeNodeSeveralSubtreesWithAllDeepest();
        Set<TreeNode> result2 = treeNodeService.subtreesWithAllDeepest(treeNodeWithSeveralSubtreesWithAllDeepest);

        result2.forEach(treeNode -> System.out.println(String.format("[%s, %s, %s]", treeNode.val,
                treeNode.left != null ? treeNode.left.val : "null",
                treeNode.right != null ? treeNode.right.val : "null")));
        System.out.println("-------------------------");

        TreeNode treeNodeWithSeveralSubtreesWithNullNode = treeNodeService.generateTreeNodeWithNullNode();
        Set<TreeNode> result3 = treeNodeService.subtreesWithAllDeepest(treeNodeWithSeveralSubtreesWithNullNode);

        result3.forEach(treeNode -> System.out.println(String.format("[%s, %s, %s]", treeNode.val,
                treeNode.left != null ? treeNode.left.val : "null",
                treeNode.right != null ? treeNode.right.val : "null")));
        System.out.println("-------------------------");

        TreeNode treeNodeWithOneNode = treeNodeService.generateTreeNodeWithOneNode();
        Set<TreeNode> result4 = treeNodeService.subtreesWithAllDeepest(treeNodeWithOneNode);

        result4.forEach(treeNode -> System.out.println(String.format("[%s, %s, %s]", treeNode.val,
                treeNode.left != null ? treeNode.left.val : "null",
                treeNode.right != null ? treeNode.right.val : "null")));
        System.out.println("-------------------------");
    }
}
